import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Hashtable;

public class JalaDictionary<K, V> extends Dictionary<K, V> {

    List<Entry<K, V>> tab;
    Set<K> keys;

    public JalaDictionary() {
        tab = new ArrayList<>();
        keys = new HashSet<>();
    }

    @Override
    public int size() {
        return tab.size();
    }

    @Override
    public boolean isEmpty() {
        return tab.size() == 0;
    }

    @Override
    public Enumeration<K> keys() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Enumeration<V> elements() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public V get(Object key) {
        for (Entry<K, V> element : tab) {
            if (element.getKey().equals(key)) {
                return (V) element.value;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        if (key == null) {
            return null;
        }
        if (keys.add(key)) {
            int hash = key.hashCode();
            tab.add(new Entry<K, V>(hash, key, value, null));
            return value;
        }
        return null;
    }

    @Override
    public V remove(Object key) {
        for (Entry<K, V> element : tab) {
            if (element.getKey().equals(key)) {
                tab.remove(element);
                keys.remove(key);
                resetEntriesNext();
                return (V) element.value;
            }
        }
        return null;
    }

    public void setNext(Entry entry) {
        if (tab.size() > 1) {
            tab.get(tab.indexOf(entry) - 1).next = tab.get(tab.indexOf(entry));
        }

    }

    public void resetEntriesNext() {
        if (isEmpty()) {
            return;
        }
        if (tab.size() == 1) {
            tab.get(0).next = null;
            return;
        }
        for (int i = 0; i < tab.size() - 1; i++) {
            tab.get(i).next = tab.get(i + 1);

        }
        tab.get(tab.size() - 1).next = null;
    }

    private static class Entry<K, V> implements Map.Entry<K, V> {
        final int hash;
        final K key;
        V value;
        Entry<K, V> next;

        protected Entry(int hash, K key, V value, Entry<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public V setValue(V value) {
            if (value == null)
                throw new NullPointerException();

            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        public boolean equals(Object o) {
            if (!(o instanceof Map.Entry))
                return false;
            Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;

            return (key == null ? e.getKey() == null : key.equals(e.getKey()))
                    && (value == null ? e.getValue() == null : value.equals(e.getValue()));
        }

        public int hashCode() {
            return hash ^ Objects.hashCode(value);
        }

        public String toString() {
            return key.toString() + "=" + value.toString();
        }
    }
}
