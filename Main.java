import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    
    public static void main(String[] args) {
        JalaDictionary<String,Integer> dictionary = new JalaDictionary<>();
        
        dictionary.put("1", 1);
        dictionary.put("2", 2);
        dictionary.put("3", 3);
        dictionary.put("4", 4);

        System.out.println(dictionary.get("1"));
        System.out.println(dictionary.remove("2"));
        System.out.println(dictionary.get("2"));
        System.out.println(dictionary.put("1", 4));
        System.out.println(dictionary.keys);
    }
}
